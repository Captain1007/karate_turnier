app
    .config(['$stateProvider', '$urlRouterProvider', '$logProvider', '$locationProvider', '$authProvider', 'NotificationProvider', '$translateProvider', '$uiViewScrollProvider',
        function ($stateProvider, $urlRouterProvider, $logProvider, $locationProvider, $authProvider, NotificationProvider, $translateProvider, $uiViewScrollProvider) {
            $urlRouterProvider.otherwise("/");

            $authProvider.loginUrl = '/api/authenticate';
            $authProvider.signupUrl = '/auth/notpossible';
            $authProvider.tokenType = '';

            $uiViewScrollProvider.useAnchorScroll();
            NotificationProvider.setOptions({
                delay: 4000,
                startTop: 20,
                startRight: 20,
                verticalSpacing: 20,
                horizontalSpacing: 20,
                positionX: 'right',
                positionY: 'top'
            });


            // $translateProvider.useStaticFilesLoader({
                // prefix: 'app/language/locale-',
                // suffix: '.json'
            // }).registerAvailableLanguageKeys(['en', 'de'], {
                // 'en_*': 'en',
                // 'de_*': 'de'
            // }).determinePreferredLanguage();

            $logProvider.debugEnabled(true);

            // use the HTML5 History API
            $locationProvider.html5Mode(true);

            $stateProvider
                //------------------------------
                // host LIST
                //------------------------------

                .state('home', {
                    url: '/',
                    templateUrl: 'app/page/home.html',
                    module: 'public'
                })
        }]);