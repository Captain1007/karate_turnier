var app = angular.module('app', [
    'ngAnimate',
    'ngMessages',
    'pascalprecht.translate',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'ngFileUpload',
    'satellizer',
    'ngTable',
    'ui-notification'
])

var compareTo = function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
};

app.directive("compareTo", compareTo);

app.run(function ($rootScope, $state, $document, $window, Notification) {
    FastClick.attach(document.body);
});